import pygame
import os

_sound_library = {}

CLICK = 'res/sounds/PM_FSSF_UI_BEEP_14.ogg'
HOVER = 'res/sounds/PM_FN_Objects_Items_Interactions_Menus_125.ogg'

def play_sound(path):
    global _sound_library
    sound = _sound_library.get(path)
    if not sound:
        canonicalized_path = path.replace('/', os.sep).replace('\\', os.sep)
        sound = pygame.mixer.Sound(canonicalized_path)
        _sound_library[path] = sound
    sound.play()
