import pygame
import gui
import game
import images
import numpy
import utils

MODE_ACTIVE = 1
MODE_DISABLED = 0


class GameScreen:
    def __init__(self, config = dict()):
        self.mode = MODE_ACTIVE
        self.state = 'default'
        self.buttons = []
        self.inputs = []
        self.units = []
        self.textboxes = []
        self.config = config
        self.activeButton = None
        self.moveCamera = 0
        self.autoredraw = False

        self.screenWidth, self.screenHeight = pygame.display.get_surface().get_size()
        self.rightPanelWidth = max(int(self.screenWidth/4), 200)

        self.reset()

    def draw(self):
        self.units = sorted(self.units, key=lambda item: item.zIndex)
        for unit in self.units:
            # if unit.target:
            #     game.surface.blit(images.TARGET, numpy.subtract(unit.target, (images.TARGET.get_width()/2, images.TARGET.get_height()/2)))

            unit.draw()

        for btn in self.buttons:
            btn.draw()
        for input in self.inputs:
            input.draw()
        for textbox in self.textboxes:
            textbox.draw()
        game.flush()

    def call(self, event):
        if not self.handleInputs(event):
            self.handleButtons(event)

        if event.type == pygame.MOUSEMOTION:
            for unit in self.units:
                unit.hover = unit.rect.collidepoint(utils.displayPointToSurface(event.pos))

    def load(self, config):
        self.config = config

    def tick(self):
        if self.moveCamera:
            delta = [0, 0]
            if self.moveCamera & gui.CAMERA_MOVE_UP:
                delta[1] -= game.CAMERA_SPEED
            if self.moveCamera & gui.CAMERA_MOVE_DOWN:
                delta[1] += game.CAMERA_SPEED
            if self.moveCamera & gui.CAMERA_MOVE_LEFT:
                delta[0] -= game.CAMERA_SPEED
            if self.moveCamera & gui.CAMERA_MOVE_RIGHT:
                delta[0] += game.CAMERA_SPEED
            game.camera.move(delta)

        for unit in self.units:
            unit.update()
        for input in self.inputs:
            if input.state == gui.STATE_ACTIVE:
                input.tick()
        if self.autoredraw:
            self.redraw()

    def redraw(self):
        self.clear()
        self.draw()

    def reset(self):
        for input in self.inputs:
            input.text = ''
            input.blinkTimeout = 15
            input.blinkState = False
            input.active = False
        self.activeButton = None
        for btn in self.buttons:
            btn.hoverOff()
            if not btn.radio:
                btn.activeOff()

    def clear(self):
        pass

    def handleInputs(self, event):
        handled = False
        for input in self.inputs:
            if input.handle_event(event):
                handled = True
                break

        if self.mode != MODE_DISABLED:
            for input in self.inputs:
                input.draw()
        return handled

    def handleButtons(self, event):

        redraw = []

        if event.type == pygame.MOUSEMOTION and not self.activeButton:
            # <Event(4-MouseMotion {'rel': (0, -1), 'buttons': (0, 0, 0), 'pos': (2, 0)})>
            # print(event)
            for btn in self.buttons:
                if btn.rect.collidepoint(event.pos):
                    btn.hoverOn()
                    redraw.append(btn)
                elif btn.state == gui.STATE_HOVER:
                    btn.hoverOff()
                    redraw.append(btn)

        elif event.type == pygame.MOUSEBUTTONDOWN:
            for btn in self.buttons:
                if btn.state != gui.STATE_DISABLED and btn.rect.collidepoint(event.pos):
                    if btn.checkbox:
                        if btn.state == gui.STATE_ACTIVE:
                            btn.activeOff(True)
                        else:
                            btn.activeOn(True)
                    else:
                        btn.activeOn(True)
                    self.activeButton = btn
                    redraw.append(btn)
                    break

        elif event.type == pygame.MOUSEBUTTONUP:
            if self.activeButton and self.activeButton.state != gui.STATE_DISABLED:
                for btn in self.buttons:
                    if btn == self.activeButton:
                        if not btn.checkbox:
                            btn.activeOff()
                        redraw.append(btn)
                        if btn.rect.collidepoint(event.pos):
                            if btn.radio and not btn.checkbox:
                                btn.activeOn()
                            else:
                                btn.hoverOn()
                            btn.click(self, btn)
                    elif self.activeButton.radio and btn.radio == self.activeButton.radio and not btn.checkbox:
                        btn.activeOff()
                        redraw.append(btn)
                self.activeButton = None

        elif event.type == pygame.KEYDOWN:
            if self.activeButton:
                self.activeButton.activeOff()
                redraw.append(self.activeButton)
                self.activeButton = None
            for btn in self.buttons:
                if btn.key == event.key:
                    btn.activeOn()
                    redraw.append(btn)
                    self.activeButton = btn
                    btn.click(self, btn)
                    break

        if self.mode != MODE_DISABLED:
            for btn in redraw:
                btn.draw()


from screens import *
