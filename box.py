

class Box:
    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    def is_collide(self, box):
        if self.right < box.left:
            return False
        if self.bottom < box.top:
            return False
        if self.left > box.right:
            return False
        if self.top > box.bottom:
            return False
        return True

    @property
    def top(self):
        return self.y

    @property
    def bottom(self):
        return self.y+self.h

    @property
    def left(self):
        return self.x

    @property
    def right(self):
        return self.x+self.w

    @property
    def center(self):
        return round(self.x+self.w/2), round(self.y+self.h/2)
