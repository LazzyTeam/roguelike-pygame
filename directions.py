import tile

N = 'north'
S = 'south'
E = 'east'
W = 'west'
NW = 'northwest'
Sw = 'southwest'
NE = 'northeast'
SE = 'southeast'

TILE = {
    'north': (0, -tile.TILE_SIZE),
    'south': (0, tile.TILE_SIZE),
    'east': (tile.TILE_SIZE, 0),
    'west': (-tile.TILE_SIZE, 0),
    'northwest': (-tile.TILE_SIZE, -tile.TILE_SIZE),
    'southwest': (-tile.TILE_SIZE, tile.TILE_SIZE),
    'northeast': (tile.TILE_SIZE, -tile.TILE_SIZE),
    'southeast': (tile.TILE_SIZE, tile.TILE_SIZE),
}

