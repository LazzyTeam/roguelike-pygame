import game
import pygame
import images
import copy
import unit
import colors

TILE_SIZE = 12


class Tile:
    # this is a generic object: the player, a monster, an item, the stairs...
    # it's always represented by a character on screen.
    def __init__(self, x, y, tile, zIndex = 0, neighbours = tuple() ):
        self.x = x
        self.y = y
        self.char, self.fgColor, self.bgColor, self.transparent, self.blocked, self.connected = tile
        self.zIndex = zIndex
        if self.connected:
            self.char = connect(neighbours)
        self.surface = images.get_tile(self.char, self.fgColor, self.bgColor)
        self.moving = (0, 0)
        self.interactive = False

    def move(self, dx, dy):
        # move by the given amount
        self.x += dx
        self.y += dy

    def draw(self):
        self.clear()
        game.surface.blit(self.surface, self.rect)

    def clear(self):
        pygame.draw.rect(game.surface, self.bgColor, self.rect)

    @property
    def rect(self):
        return pygame.Rect((self.x, self.y, TILE_SIZE, TILE_SIZE))

    def update(self):
        pass


class MultitileObject(Tile):
    def __init__(self, x, y, tile_map, zIndex = 0):
        super().__init__(x, y, tile_map[0][0], zIndex)
        self.tiles = []
        self.x = x
        self.y = y
        self.h = len(tile_map) * TILE_SIZE
        self.w = TILE_SIZE
        for i in range(len(tile_map)):
            if len(tile_map[i]) * TILE_SIZE > self.w:
                self.w = len(tile_map[i]) * TILE_SIZE
            for j in range(len(tile_map[i])):
                neighbours = tuple()
                try:
                    bottom = i < len(tile_map)-1 and tile_map[i+1][j][4]
                    top = i > 0 and tile_map[i-1][j][4]
                    left = j > 0 and tile_map[i][j-1][4]
                    right = j < len(tile_map[i])-1 and tile_map[i][j+1][4]
                    neighbours = (right, left, top, bottom)
                except:
                    # print(tile_map[i - 1])
                    pass
                t = Tile(j, i, tile_map[i][j], zIndex, neighbours)
                if tile_map[i][j][0] == 'door':
                    opened = ('.', colors.LightSteelBlue, colors.Black, True, False, False)
                    t = unit.Door({
                        'closed': t,
                        'opened': Tile(j, i, opened, zIndex, neighbours)
                    }, 'closed')
                elif tile_map[i][j][0] == 'control_panel':
                    t = unit.ControlPanel(Tile(j, i, ('wall_top', colors.Aqua, colors.Black, False, True, False)))
                self.tiles.append(t)

    def draw(self):
        for tile in self.tiles:
            if tile.__class__ != Tile:
                tile = tile.tile
            t = copy.copy(tile)
            t.x = self.x+t.x*TILE_SIZE
            t.y = self.y+t.y*TILE_SIZE
            t.draw()

    def clear(self):
        for tile in self.tiles:
            t = copy.copy(tile)
            t.x = self.x+t.x*TILE_SIZE
            t.y = self.y+t.y*TILE_SIZE
            t.clear()

    @property
    def rect(self):
        return pygame.Rect((self.x, self.y, self.w, self.h))

    def update(self):
        for tile in self.tiles:
            tile.update()


def connect(neighbours):
    right, left, top, bottom = neighbours
    if right and left and top and bottom:
        return '┼'
    elif right and left and not top and not bottom:
        return '─'
    elif not right and not left and top and bottom:
        return '│'
    elif not right and not left and not top and not bottom:
        return '*'
    elif right and not left and not top and bottom:
        return '┌'
    elif right and not left and top and bottom:
        return '├'
    elif right and left and not top and bottom:
        return '┬'
    elif not right and left and not top and not bottom:
        return '>'
    elif not right and left and top and not bottom:
        return '┘'
    elif right and left and top and not bottom:
        return '┴'
    elif not right and not left and not top and bottom:
        return '^'
    elif not right and left and not top and bottom:
        return '┐'
    elif not right and left and top and bottom:
        return '┤'
    elif not right and not left and top and not bottom:
        return 'v'
    elif right and not left and not top and not bottom:
        return '<'
    elif right and not left and top and not bottom:
        return '└'

