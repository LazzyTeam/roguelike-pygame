from tile import MultitileObject
from box import Box
from tilemap import TileMap
import random
import colors

class Ship(MultitileObject):
    def __init__(self, x, y, tile_map):
        super().__init__(x, y, tile_map)


def load():
    tile_map = []
    f = open('res/data/ships.txt')
    lines = f.readlines()
    for line in lines:
        tile_row = []
        for c in line:
            transparent = True
            blocked = False
            connected = False
            color = colors.LightSlateGray
            bgColor = colors.Black
            if c == 'I':
                color = colors.LightBlue
                blocked = True
                connected = True
                transparent = False
            elif c == 'E':
                color = colors.Yellow
                blocked = True
                transparent = False
            elif c == '=':
                color = colors.LightSteelBlue
                c = 'door'
                transparent = False
                blocked = True
            elif c == '-':
                color = colors.AliceBlue
                blocked = True
                transparent = False
                c = 'control_panel'
            elif c == 'R':
                color = colors.OrangeRed

            tile_row.append((c, color, bgColor, transparent, blocked, connected))
        tile_map.append(tile_row)
    return Ship(0, 0, tile_map)


def generate():
    # генерим рандом коробки
    box_count = random.randint(2, 7)
    boxes = []
    for i in range(0, box_count):
        first = i == 0
        x = random.randint(-7, 7)
        y = 0 if first else random.randint(0, 10)
        w = 5 if first else random.randint(2, 5)
        h = 5 if first else random.randint(2, 5)
        boxes.append(Box(x, y, w, h))

    # соединяем все коробки с центральной проходами
    center_box = boxes[0]
    for box in boxes[1:]:
        if box.is_collide(center_box):
            continue

        if box.right < center_box.left+1:
            x = box.right-2
            w = center_box.left - box.right + 2
        elif box.left > center_box.right-1:
            x = center_box.right-2
            w = box.left - center_box.right + 2
        else:
            x = min(box.left, center_box.left)
            w = max(box.right, center_box.right) - x

        if box.bottom < center_box.top+1:
            y = box.bottom-2
            h = center_box.top - box.bottom + 2
        elif box.top > center_box.bottom-1:
            y= center_box.bottom-2
            h = box.top - center_box.bottom + 2
        else:
            y = min(box.top, center_box.top)
            h = max(box.bottom, center_box.bottom) - y

        boxes.append(Box(x, y, w, h))

    # заполняем подложку
    bottom_tiles = TileMap()
    for box in boxes:
        for i in range(box.left, box.right):
            for j in range(box.top, box.bottom):
                bottom_tiles.set(i, j, True)
                # зеркалим
                bottom_tiles.set(i, -j, True)

    tiles = TileMap()
    for i in range(bottom_tiles.min_x, bottom_tiles.max_x+1):
        for j in range(bottom_tiles.min_y, bottom_tiles.max_y+1):
            if not bottom_tiles.get(i, j):
                tiles.set(i, j, ' ')
            else:
                bottom = bottom_tiles.get(i+1, j)
                top = bottom_tiles.get(i-1, j)
                right = bottom_tiles.get(i, j+1)
                left = bottom_tiles.get(i, j-1)

                if right and left and top and bottom:
                    c = '┼'
                elif right and left and not top and not bottom:
                    c = '─'
                elif not right and not left and top and bottom:
                    c = '│'
                elif not right and not left and not top and not bottom:
                    c = '*'
                elif right and not left and not top and bottom:
                    c = '┌'
                elif right and not left and top and bottom:
                    c = '├'
                elif right and left and not top and bottom:
                    c = '┬'
                elif not right and left and not top and not bottom:
                    c = '>'
                elif not right and left and top and not bottom:
                    c = '┘'
                elif right and left and top and not bottom:
                    c = '┴'
                elif not right and not left and not top and bottom:
                    c = '^'
                elif not right and left and not top and bottom:
                    c = '┐'
                elif not right and left and top and bottom:
                    c = '┤'
                elif not right and not left and top and not bottom:
                    c = 'v'
                elif right and not left and not top and not bottom:
                    c = '<'
                elif right and not left and top and not bottom:
                    c = '└'
                else:
                    c = '.'
                tiles.set(i, j, c)

    char_map = []
    color_map = []
    for i in range(tiles.min_x, tiles.max_x+1):
        char_row = []
        color_row = []
        for j in range(tiles.min_y, tiles.max_y+1):
            char_row.append(tiles.get(i, j))
            color_row.append(colors.White)
        char_map.append(char_row)
        color_map.append(color_row)
    return Ship(0, 0, char_map, color_map)
