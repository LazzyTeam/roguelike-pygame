import pygame
import colors
import numpy
import game
import time
import sound

STATE_DEFAULT = 0
STATE_HOVER = 1
STATE_ACTIVE = 2
STATE_DISABLED = -1

CAMERA_MOVE_UP = 1
CAMERA_MOVE_DOWN = 2
CAMERA_MOVE_LEFT = 4
CAMERA_MOVE_RIGHT = 8

class Camera:
    def __init__(self, pos):
        self.pos = pos

    def move(self, delta):
        pos = (self.pos[0] + delta[0], self.pos[1] + delta[1])
        if game.display.get_width()/2 <= pos[0] < game.surface.get_width()-game.display.get_width()/2 and game.display.get_height()/2 <= pos[1] < game.surface.get_height() - game.display.get_height()/2:
            self.pos = pos

    def getBounds(self):
        return pygame.Rect((self.pos[0]-game.display.get_width()/2, self.pos[1]-game.display.get_height()/2, game.display.get_width(), game.display.get_height()))


class TextBox:
    def __init__(self, pos, text, color = colors.LightSteelBlue, surface=False):
        self.text = text
        self.pos = pos
        self.color = color
        self.surface = surface
        self.render = game.font.render(text, False, color)

    def draw(self):
        if self.surface:
            self.surface.blit(self.render, self.pos)
        else:
            game.staticSurface.blit(self.render, self.pos)


class BaseInput:
    def __init__(self, key=None, state=STATE_DEFAULT):
        self.state = state
        self.key = key

    def hoverOn(self):
        if self.state == STATE_DISABLED:
            return
        if self.state == STATE_DEFAULT:
            self.state = STATE_HOVER
            # sound.play_sound(sound.HOVER)

    def hoverOff(self):
        if self.state == STATE_DISABLED:
            return
        if self.state == STATE_HOVER:
            self.state = STATE_DEFAULT

    def activeOn(self, click=False):
        if self.state == STATE_DISABLED:
            return
        self.state = STATE_ACTIVE
        if click:
            sound.play_sound(sound.CLICK)

    def activeOff(self, click=False):
        if self.state == STATE_DISABLED:
            return
        self.state = STATE_DEFAULT
        if click:
            sound.play_sound(sound.CLICK)


class BaseButton(BaseInput):
    def __init__(self, click=lambda screen, btn: None, state=STATE_DEFAULT, radio=None, key=None):
        super().__init__(key, state)
        self.click = click
        self.radio = radio


class ImageButton(BaseButton):
    def __init__(self, pos, image, imageHover, imageActive,
                 click=lambda screen, btn: None, state=STATE_DEFAULT, radio=None, key=None):
        super().__init__(click, state, radio, key)
        self.pos = pos
        self.image = image
        self.imageHover = imageHover
        self.imageActive = imageActive
        self.rect = pygame.Rect(pos[0], pos[1], image.get_width(), image.get_height())

    def draw(self):
        image = self.image
        if self.state == STATE_HOVER:
            image = self.imageHover
        if self.state == STATE_ACTIVE:
            image = self.imageActive

        game.staticSurface.blit(image, self.pos)


class Button(BaseButton):

    def __init__(self, rect, text, click = lambda screen, btn: None,
                 mainColor=colors.White, hoverColor=colors.Aqua, activeColor=colors.LightGreen, backgroundColor=colors.DarkSlateGray,
                 state=STATE_DEFAULT, radio=None, icon=None, key=None, align='center', checkbox=False):
        super().__init__(click, state, radio, key)
        self.rect = pygame.Rect(rect)
        self.text = text
        self.mainColor = mainColor
        self.backgroundColor = backgroundColor
        self.hoverColor = hoverColor
        self.activeColor = activeColor
        self.icon = icon
        self.align = align
        self.checkbox = checkbox

    def draw(self):

        color = self.mainColor
        if self.state == STATE_HOVER:
            color = self.hoverColor
        if self.state == STATE_ACTIVE:
            color = self.activeColor
        if self.state == STATE_DISABLED:
            color = colors.Gray

        pygame.draw.rect(game.staticSurface, self.backgroundColor, self.rect)
        pygame.draw.rect(game.staticSurface, color, self.rect, 2)
        render = game.font.render(self.text, False, color)

        if self.icon:
            posIcon = numpy.array(self.rect.topleft) + numpy.array((10, 5))
            game.staticSurface.blit(self.icon, posIcon)
            posText = (posIcon[0]+self.icon.get_width()+5, self.rect.center[1]-render.get_height()/2)
        else:
            if self.align == 'center':
                posText = tuple(numpy.subtract(self.rect.center, (render.get_width()/2, render.get_height()/2)))
            elif self.align == 'left':
                posText = (self.rect.left+5, self.rect.centery-render.get_height()//2)

        game.staticSurface.blit(render, posText)


class SliderInput(BaseInput):
    def __init__(self, rect, start, end, value, key=None, state=STATE_DEFAULT, onChange=lambda value: None):
        super().__init__(key, state)
        rect = pygame.Rect(rect)
        self.rect = rect
        self.start = start
        self.end = end
        self.value = value
        self.step = (end-start)/(rect.width-45) # values per pixel
        self.color = colors.White
        self.colorBackground = colors.Black
        self.colorLine = colors.White
        self.colorLineActive = colors.LightSteelBlue
        self.lineLeft = self.rect.left + 40
        self.lineRight = self.rect.right - 5
        self.onChange = onChange

    def draw(self):
        value_surface = game.font.render(str(self.value), True, self.color)
        pygame.draw.rect(game.staticSurface, self.colorBackground, self.rect)
        game.staticSurface.blit(value_surface, (self.rect.left+5, self.rect.center[1]-value_surface.get_height()/2))

        color = self.colorLineActive if self.state == STATE_ACTIVE else self.colorLine
        line = (self.lineLeft, self.rect.center[1]-2, self.lineRight-self.lineLeft, 4)
        pygame.draw.rect(game.staticSurface, color, line)

        pos = (self.value-self.start)/self.step
        pusher = (self.lineLeft+pos-3, self.rect.top, 6, self.rect.height)
        pygame.draw.rect(game.staticSurface, color, pusher)

    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            if self.rect.collidepoint(event.pos):
                self.state = STATE_ACTIVE
                self._updateValue(event.pos[0])
            else:
                self.state = STATE_DEFAULT
        elif self.state == STATE_ACTIVE and event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            self.state = STATE_DEFAULT
        elif self.state == STATE_ACTIVE and event.type == pygame.MOUSEMOTION:
            self._updateValue(event.pos[0])

    def _updateValue(self, x):

        if self.lineLeft <= x <= self.lineRight:
            self.value = self.start+round(self.step * (x - self.lineLeft))
        elif x < self.lineLeft:
            self.value = self.start
        elif x > self.lineRight:
            self.value = self.end
        self.onChange(self, self.value)

    def tick(self):
        return True


BLINK_PERIOD = 0.5


class TextInput(BaseInput):

    def __init__(self, x, y, w, h, text='', key=None, state=STATE_DEFAULT, onChange=lambda value: None):
        super().__init__(key, state)
        self.rect = pygame.Rect(x, y, w, h)
        self.color = colors.White
        self.backgroundColor = colors.Black
        self.text = text
        self.txt_surface = game.font.render(text, True, self.color)
        self.blinkTimeout = BLINK_PERIOD
        self.blinkState = True
        self.blinkLast = 0
        self.onChange = onChange

    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            if self.rect.collidepoint(event.pos):
                self.state = STATE_ACTIVE
            else:
                self.state = STATE_DEFAULT
            # Change the current color of the input box.
            self.color = colors.LightSteelBlue if self.state == STATE_ACTIVE else colors.White
        elif event.type == pygame.KEYDOWN:
            if self.state == STATE_ACTIVE:
                # if event.key == pygame.K_RETURN:
                #     print(self.text)
                #     self.text = ''
                if event.key == pygame.K_BACKSPACE:
                    self.text = self.text[:-1]
                    self.onChange(self.text)
                elif event.key != pygame.K_ESCAPE:
                    self.text += event.unicode
                    self.onChange(self.text)
                return True

        return False

    def draw(self):
        self.txt_surface = game.font.render(self.text, True, self.color)
        # Blit the rect.
        pygame.draw.rect(game.staticSurface, self.backgroundColor, self.rect)
        pygame.draw.rect(game.staticSurface, self.color, self.rect, 2)
        # Blit the text.
        game.staticSurface.blit(self.txt_surface, (self.rect.x+5, self.rect.y+5))

    def tick(self):
        needFlush = False
        now = time.time()
        if self.blinkTimeout > 0:
            self.blinkTimeout -= now - self.blinkLast
        else:
            self.blinkState = not self.blinkState
            self.blinkTimeout = BLINK_PERIOD

            cursor = game.font.render(' ', True, colors.White, colors.White if self.blinkState else colors.Black)
            game.staticSurface.blit(cursor, (self.rect.x+self.txt_surface.get_width()+7, self.rect.y+5))
            needFlush = True
        self.blinkLast = time.time()
        return needFlush


class Tabs:
    def __init__(self, rect, names, active=0,
                 color=colors.White, hoverColor=colors.LightSteelBlue, activeColor=colors.Black,
                 bgColor=colors.Black, bgHoverColor=colors.Black, bgActiveColor=(200,200,200),
                 borderColor=colors.LightSteelBlue):
        rect = pygame.Rect(rect)
        self.rect = rect
        self.names = names
        self.active = active
        self.color = color
        self.hoverColor = hoverColor
        self.activeColor = activeColor
        self.bgColor = bgColor
        self.bgHoverColor = bgHoverColor
        self.bgActiveColor = bgActiveColor
        self.borderColor = borderColor
        self.rects = []

        self.surface = pygame.Surface((rect.width, rect.height))
        self.textboxes = []
        self.buttons = []
        self.inputs = []
        for i in range(len(names)):
            self.textboxes.append([])
            self.buttons.append([])
            self.inputs.append([])
        self._render()

    def _render(self):
        self.rects = []
        self.surface.fill(colors.Black)
        i = 0
        x = 1
        pygame.draw.rect(self.surface, self.borderColor, self.rect, 1)
        for name in self.names:
            name = '< ' + name + ' >'
            c = self.activeColor if self.active == i else self.color
            bg = self.bgActiveColor if self.active == i else self.bgColor
            rend = game.font.render(name, False, c)
            rect = pygame.Rect(x, 1, rend.get_width()+10, 30)
            self.rects.append(rect)
            pygame.draw.rect(self.surface, bg, rect)
            self.surface.blit(rend, (x+5, 6))
            i += 1
            x += rend.get_width()+10
            pygame.draw.line(self.surface, self.borderColor, (x, 0), (x, 30))
            x += 1
        for box in self.textboxes[self.active]:
            box.draw()

    def change(self, tab):
        self.active = tab
        self._render()

    def draw(self):
        game.staticSurface.blit(self.surface, self.rect)

    def reset(self):
        for tab in self.inputs:
            for input in tab:
                input.text = ''
        for tab in self.buttons:
            for button in tab:
                button.state = STATE_DEFAULT

    def checkCollide(self, pos):
        if pos[1] <= 30:
            i = 0
            for rect in self.rects:
                if rect.collidepoint(pos[0], pos[1]):
                    return i
                i += 1
        return -1

