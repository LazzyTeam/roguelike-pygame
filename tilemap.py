

class TileMap:
    def __init__(self):
        self.tiles = dict()

    def set(self, x, y, v):
        if not x in self.tiles:
            self.tiles[x] = dict()
        self.tiles[x][y] = v

    def exist(self, x, y):
        return x in self.tiles and y in self.tiles[x]

    def get(self, x, y):
        if self.exist(x, y):
            return self.tiles[x][y]
        else:
            return None

    def clear(self):
        self.tiles = dict()

    @property
    def min_x(self):
        return min(self.tiles.keys())

    @property
    def max_x(self):
        return max(self.tiles.keys())

    @property
    def min_y(self):
        min_y = 999999
        for i in self.tiles:
            m = min(self.tiles[i].keys())
            if m < min_y:
                min_y = m
        return min_y

    @property
    def max_y(self):
        max_y = -999999
        for i in self.tiles:
            m = max(self.tiles[i].keys())
            if m > max_y:
                max_y = m
        return max_y