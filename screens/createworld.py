from gamescreen import GameScreen
import game
import colors
import gui
from translate import _
import pygame
import savefile


class CreateWorld(GameScreen):
    def __init__(self):
        super().__init__()
        self.worldConfig = savefile.Config()
        buttonWidth = 150
        buttonHeight = 40

        nameTextBox = gui.TextBox((10, 15), _('World name:'), colors.White)
        self.textboxes.append(nameTextBox)
        nameInput = gui.TextInput(200, 10, self.screenWidth-220, 30,
                                  onChange=self.setName)
        self.inputs.append(nameInput)

        backBtn = gui.Button((self.screenWidth-buttonWidth*2-20, self.screenHeight-buttonHeight-10, buttonWidth, buttonHeight), _('Back'),
                                lambda screen, btn: game.change_screen('MainMenu'), key=pygame.K_ESCAPE)
        self.buttons.append(backBtn)
        self.createBtn = gui.Button((self.screenWidth-buttonWidth-10, self.screenHeight-buttonHeight-10, buttonWidth, buttonHeight), _('Create'),
                                lambda screen, btn: screen.confirmCreate(), state=gui.STATE_DISABLED)
        self.buttons.append(self.createBtn)


    def reset(self):
        super().reset()

    def clear(self):
        super().clear()
        game.staticSurface.fill(colors.Black)

    def call(self, event):
        super().call(event)

    def draw(self):
        if self.mode == 'creating':
            rend = game.font.render('Creating new world...', False, colors.LightSteelBlue)
            game.staticSurface.blit(rend, (self.screenWidth//2-rend.get_width()//2, self.screenHeight//2-rend.get_height()//2))
        else:
            rend = game.font.render('other options will be added soon...', False, colors.White)
            game.staticSurface.blit(rend, (10, 50))
            super().draw()

    def setName(self, value):
        self.worldConfig.name = value
        if self.worldConfig.validate():
            self.createBtn.state = gui.STATE_DEFAULT
        else:
            self.createBtn.state = gui.STATE_DISABLED
        self.createBtn.draw()

    def confirmCreate(self):
        self.mode = 'creating'
        self.redraw()
        game.flush()
        pygame.time.delay(500)
        savefile.createNewWorld(self.worldConfig)
        game.change_screen('MainMenu')
