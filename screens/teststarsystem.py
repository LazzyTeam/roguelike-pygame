import colors
import game
from gamescreen import GameScreen
import gui
import pygame
import math


class TestStarsystem(GameScreen):
    def __init__(self):
        super().__init__()
        self.reset()
        btnWidth = 250
        btnHeight = 40
        btn = gui.Button((self.screenWidth-btnWidth-10, 10, btnWidth, btnHeight), '[Esc] Leave control',
                         key=pygame.K_ESCAPE, click=lambda screen, btn: game.change_screen('TestScreen'))
        self.buttons.append(btn)

        self.angles = [0 for i in range(10)]

    def reset(self):
        super().reset()

    def clear(self):
        super().clear()

    def call(self, event):
        super().call(event)

    def draw(self):
        pygame.draw.circle(game.surface, colors.LightYellow, (self.screenWidth//2, self.screenHeight//2), 10)
        width = self.screenWidth-200
        for i in range(10):
            height = width/3
            pygame.draw.arc(game.surface, colors.LightSteelBlue, (self.screenWidth//2-width//2, self.screenHeight//2-height//2, int(width), int(height)), -3.2, 3.2, 1)
            width *= 0.8
        super().draw()

    def tick(self):
        game.surface.fill(colors.Black)
        self.draw()
        speed = 0.001
        r = (self.screenWidth-200)/2

        for i in range(10):
            x = r*math.cos(self.angles[i])
            y = r*math.sin(self.angles[i])/3
            x += self.screenWidth/2
            y += self.screenHeight/2
            pygame.draw.circle(game.surface, colors.LightYellow, (int(x),int(y)), 5)
            self.angles[i] += speed
            r *= 0.8
            speed *= 1.3

