from gamescreen import GameScreen
import savefile
import game
import colors
import pygame
import gui
from translate import _


class CreateCharacter(GameScreen):

    def __init__(self):
        self.tab = 0
        self.player = savefile.Player()
        self.selected = 0
        self.tabs = None
        super().__init__()

        buttonWidth = 150
        buttonHeight = 40
        self.tabs = gui.Tabs((0, 0, self.screenWidth, self.screenHeight),
                             ('Basic', 'Scenario', 'Attributes', 'Skills', 'Perks', 'Appearance'), active=self.tab)

        backBtn = gui.Button((self.screenWidth - buttonWidth - 10, self.screenHeight - buttonHeight - 10,
                              buttonWidth, buttonHeight), _('Back'),
                             lambda screen, btn: game.change_screen('WorldPreview', screen.config), key=pygame.K_ESCAPE)
        self.tabs.buttons[0].append(backBtn)
        nameBox = gui.TextBox((10, 40), _('Name:'), surface=self.tabs.surface)
        self.tabs.textboxes[0].append(nameBox)
        nameInput = gui.TextInput(100, 35, 300, 30,
                                  onChange=lambda value: game.currentScreen.changeName(value))
        self.tabs.inputs[0].append(nameInput)
        genderBox = gui.TextBox((10, 85), _('Gender:'), surface=self.tabs.surface)
        self.tabs.textboxes[0].append(genderBox)
        genderMale = gui.Button((100, 80, 150, 30), _('Male'), radio='gender', state=gui.STATE_ACTIVE,
                                click=lambda screen, btn: screen.selectGender(True))
        self.tabs.buttons[0].append(genderMale)
        genderFemale = gui.Button((250, 80, 150, 30), _('Female'), radio='gender',
                                click=lambda screen, btn: screen.selectGender(False))
        self.tabs.buttons[0].append(genderFemale)
        raceBox = gui.TextBox((10, 130), _('Race:'), surface=self.tabs.surface)
        self.tabs.textboxes[0].append(raceBox)
        raceHuman = gui.Button((100, 125, 150, 30), _('Human'), radio='race', state=gui.STATE_ACTIVE)
        self.tabs.buttons[0].append(raceHuman)
        raceBlorg = gui.Button((250, 125, 150, 30), _('Blorg'), radio='race')
        self.tabs.buttons[0].append(raceBlorg)
        raceOctopus = gui.Button((400, 125, 150, 30), _('Octopus'), radio='race')
        self.tabs.buttons[0].append(raceOctopus)

        scenarios = ('Smuggler', 'Ranger', 'Trader',)
        scenariosTexts = ('Just one more smuggler', 'Some text', 'Ololo')
        self.scenarioTexts = []
        i = 0
        for scenario in scenarios:
            text = scenariosTexts[i]
            btn = gui.Button((10+155*i, 40, 150, 30), scenario, radio='scenario', key=i,
                             state=gui.STATE_ACTIVE if i == 0 else gui.STATE_DEFAULT,
                             click=lambda screen, btn: screen.selectScenario(btn.key))
            self.tabs.buttons[1].append(btn)
            desc = gui.TextBox((10, self.screenHeight-100), text)
            self.scenarioTexts.append(desc)
            i += 1

        names = {
            'strength': _('Strength'),
            'agility': _('Agility'),
            'intelligence': _('Intelligence'),
            'charisma': _('Charisma'),
            'luck': _('Luck'),
        }
        i = 0
        for key in self.player.attributes:
            inputAttr = gui.SliderInput((160, 40+40*i, self.screenWidth-170, 30), 1, 20, 12, key=key,
                                            onChange=lambda btn, value: game.currentScreen.changeAttribute(btn.key, value))
            self.tabs.inputs[2].append(inputAttr)
            boxAttr = gui.TextBox((10, 45+40*i), names[key])
            self.tabs.textboxes[2].append(boxAttr)
            i += 1

        width = self.screenWidth//2-20
        i = 0
        for skill in self.player.skills:
            inputSkill = gui.SliderInput((200 if i%2 == 0 else width+200, 40+35*(i//2), width-200, 30), 0, 7, 0, key=skill,
                                            onChange=lambda btn, value: game.currentScreen.changeSkill(btn.key, value))
            self.tabs.inputs[3].append(inputSkill)
            boxSkill = gui.TextBox((10 if i%2 == 0 else width+10, 45+35*(i//2)), skill.capitalize())
            self.tabs.textboxes[3].append(boxSkill)
            i += 1

        perksNames = (
            ('Fastfooted', 'Night vision', 'Beautiful'),
            ('Slowfooted', 'Insomnia', 'Ugly')
        )

        x = 10
        colI = 0
        for col in perksNames:
            y = 40
            for perk in col:
                btnPerk = gui.Button((x, y, width, 30), perk,
                                     activeColor=colors.LightGreen if colI == 0 else colors.LightCoral,
                                     key=perk.lower(), radio='perk', checkbox=True,
                                     click=lambda screen, btn: screen.invertPerk(btn.key))
                self.tabs.buttons[4].append(btnPerk)
                y += 35
            x += self.screenWidth//2
            colI += 1

        text = gui.TextBox((10, 30), 'Dick size, etc.')
        self.tabs.textboxes[5].append(text)

        confirm = gui.Button((self.screenWidth - buttonWidth - 10, self.screenHeight - buttonHeight - 10,
                              buttonWidth, buttonHeight), _('Create'),
                             lambda screen, btn: screen.create(), key=pygame.K_RETURN)
        self.tabs.buttons[5].append(confirm)

    def selectScenario(self, i):
        self.player.scenario = i
        self.redraw()

    def changeName(self, name):
        self.player.name = name

    def selectGender(self, val):
        self.player.gender = val

    def changeAttribute(self, attr, val):
        self.player.attributes[attr] = val

    def changeSkill(self, skill, val):
        self.player.skills[skill] = val

    def invertPerk(self, perk):
        if perk in self.player.perks:
            self.player.perks.remove(perk)
        else:
            self.player.perks.append(perk)

    def create(self):
        self.mode = 0
        game.staticSurface.fill(colors.Black)
        text = game.font.render('Creating...', False, colors.LightSteelBlue)
        game.staticSurface.blit(text, (self.screenWidth//2-text.get_width()//2, self.screenHeight//2-text.get_height()//2))
        game.flush()
        self.config['world'].appendPlayer(self.player)
        pygame.time.delay(200)
        game.change_screen('WorldPreview', self.config, True)

    def reset(self):
        self.tab = 0
        if self.tabs:
            self.tabs.reset()
            self.tabchange()
        self.player = savefile.Player()
        self.selected = 0

    def draw(self):

        self.tabs.draw()

        super().draw()
        if self.tab == 1:
            pygame.draw.rect(game.staticSurface, colors.LightSteelBlue, (0, self.screenHeight-100, self.screenWidth, 100), 1)
            self.scenarioTexts[self.player.scenario].draw()

        # if self.tab == 0:
        #     self.inputsCount = 3
        #     TextInput(self, Rectangle(self, 1, 3, self.width-2, 1).setColors(bgColor=bgColors[self.selected == 0]), name = 'Character name', value = ''.join(self.unit.name)).draw()
        #     GenderInput(self, Rectangle(self, 1, 4, self.width-2, 1).setColors(bgColor=bgColors[self.selected == 1]), name = 'Gender', value = self.unit.gender).draw()
        #     LineBar(self, Rectangle(self, 1, 5, self.width-2, 1).setColors(bgColor=bgColors[self.selected == 2]), name = 'Age', value = self.unit.age, min = 5, max = 90).draw()
        # elif self.tab == 1:
        #     self.inputsCount = len(self.scenarios)
        #     BorderedRectangle(self, 1, 3, 20, self.height - 5).draw()
        #     RightPanelBorderedRectangle(self, 20, 3, self.width-22, self.height - 5).draw()
        #     i = 0
        #     fgColors = ((255,255,255), (255,155,0))
        #     for scenario in self.scenarios:
        #         self.draw_str(2, i+4, scenario, fgColors[i == self.unitScenario], bgColors[i == self.selected])
        #         i += 1
        #     self.draw_str(21, 4, self.scenariosTexts[self.selected])
        # elif self.tab == 2:
        #     self.inputsCount = 5
        #     BorderedRectangle(self, 1, 3, self.width - 2, self.height - 5).draw()
        #     LineBar(self, Rectangle(self, 2, 4, self.width - 2, 1).setColors(bgColor=bgColors[self.selected == 0]),
        #             name='Strength', value=self.unit.attributes['Strength'], min=1, max=7, textWidth=24,
        #             activeColor=(0, 255, 255), valueDisplayFunction=skillValueName).draw()
        #     LineBar(self, Rectangle(self, 2, 5, self.width - 2, 1).setColors(bgColor=bgColors[self.selected == 1]),
        #             name='Agility', value=self.unit.attributes['Agility'], min=1, max=7, textWidth=24,
        #             activeColor=(0, 255, 255), valueDisplayFunction=skillValueName).draw()
        #     LineBar(self, Rectangle(self, 2, 6, self.width - 2, 1).setColors(bgColor=bgColors[self.selected == 2]),
        #             name='Intelligence', value=self.unit.attributes['Intelligence'], min=1, max=7, textWidth=24,
        #             activeColor=(0, 255, 255), valueDisplayFunction=skillValueName).draw()
        #     LineBar(self, Rectangle(self, 2, 7, self.width - 2, 1).setColors(bgColor=bgColors[self.selected == 3]),
        #             name='Charisma', value=self.unit.attributes['Charisma'], min=1, max=7, textWidth=24,
        #             activeColor=(0, 255, 255), valueDisplayFunction=skillValueName).draw()
        #     LineBar(self, Rectangle(self, 2, 8, self.width - 2, 1).setColors(bgColor=bgColors[self.selected == 4]),
        #             name='Luck', value=self.unit.attributes['Luck'], min=1, max=7, textWidth=24,
        #             activeColor=(0, 255, 255), valueDisplayFunction=skillValueName).draw()
        #
        #     self.draw_str(3, self.height - 3, '[ Points remaining: ' + str(20 - self.attrPoints) + '/20 ]')
        # elif self.tab == 3:
        #
        #     names = [
        #         'Dodge',
        #         'Melee',
        #         'Unarmed combat',
        #         'Bashing weapons',
        #         'Cutting weapons',
        #         'Piercing weapons',
        #         'Throwing',
        #         'Firearms',
        #         'Handguns',
        #         'Shotguns',
        #         'Submachine guns',
        #         'Rifles',
        #         'Archery',
        #         'Grenade launcher',
        #         'Turrets',
        #         'Computers',
        #         'Mechanics',
        #         'Fabrication',
        #         'Electronics',
        #         'Cooking',
        #         'Carpentry',
        #         'Survival',
        #         'Traps',
        #         'Tailoring',
        #         'First Aid',
        #         'Speech',
        #         'Barter',
        #         'Swimming',
        #         'Driving',
        #         'Land driving',
        #         'Air driving',
        #         'Space driving',
        #         'Water driving'
        #     ]
        #     values = [self.unit.skills[n] for n in names]
        #     self.inputsCount = len(names)
        #
        #     self.tab3list = LineBarList(self, BorderedRectangle(self, 1, 3, self.width - 2, self.height - 5), names,
        #                                 values, 0, 7, self.selected, self.verticalScroll3, 24, skillValueName,
        #                                 (0, 255, 255))
        #     self.tab3list.draw()
        #
        #     self.draw_str(3, self.height - 3, '[ Points remaining: ' + str(30 - self.skillsPoints) + '/30 ]')
        # elif self.tab == 4:
        #     center = self.width // 2
        #
        #     perksPlus = self.perksNames[0]
        #     perksMinus = self.perksNames[1]
        #     valuesPlus = tuple([perk in self.unit.perks for perk in perksPlus])
        #     valuesMinus = tuple([perk in self.unit.perks for perk in perksMinus])
        #
        #     self.tab4lists = [
        #         CheckList(self,
        #                   BorderedRectangle(self, 1, 3, center - 1, self.height - 5),
        #                   perksPlus, valuesPlus, self.selected, self.verticalScroll40, checkedColor=(0, 255, 0)),
        #         CheckList(self,
        #                   RightPanelBorderedRectangle(self, center - 1, 3, self.width - center, self.height - 5),
        #                   perksMinus, valuesMinus, self.selected, self.verticalScroll41, checkedColor=(255, 0, 0))
        #     ]
        #
        #     self.tab4lists[not self.panel4].passive()
        #     self.inputsCount = self.tab4lists[self.panel4].itemsLength
        #
        #     self.tab4lists[0].draw()
        #     self.tab4lists[1].draw()
        #
        # elif self.tab == 5:
        #     self.inputsCount = 0
        #     self.draw_str(1, 3, 'There will be appearance settings, but now theres not')
        #     self.draw_str(1, 4, 'Press Ctrl+Enter for confirm creating')
        #
        # self.draw_str(1, self.height - 2, 'Arrows for navigate inputs, TAB and Shift+TAB or < and > to change tabs', (155,155,155), (0,0,0))
        #
        # if self.mode == 'confirmescape' or self.mode == 'confirmcreating':
        #
        #     confirmText = ('Are you wanna cancel character creating?', 'Are you confirm character creating?')[
        #                       self.mode == 'confirmcreating'] + ' ( Y / N )'
        #     boxWidth = len(confirmText)+2
        #     boxHeight = 3
        #     position = calcCenterPosition(self, boxWidth, boxHeight)
        #     BorderedRectangle(self, position[0], position[1], boxWidth, boxHeight).draw()
        #     self.draw_str(position[0]+1, position[1]+1, confirmText, (255,255,255), (55,55,55))
        #     if self.confirmButton:
        #         self.draw_char(position[0]+boxWidth-8, position[1]+1, 'Y', (0,255,0), (55,55,55))
        #     else:
        #         self.draw_char(position[0]+boxWidth-4, position[1]+1, 'N', (255,0,0), (55,55,55))

    def tabchange(self):
        self.tabs.change(self.tab)
        self.selected = 0
        self.inputs = self.tabs.inputs[self.tab]
        self.textboxes = self.tabs.textboxes[self.tab]
        self.buttons = self.tabs.buttons[self.tab]
        self.redraw()

    def tabplus(self):
        if self.tab < len(self.tabs.names)-1:
            self.tab += 1
            self.tabchange()

    def tabminus(self):
        if self.tab > 0:
            self.tab -= 1
            self.tabchange()

    def call(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_TAB:
                if event.mod & pygame.KMOD_LSHIFT:
                    self.tabminus()
                else:
                    self.tabplus()
            elif event.key == pygame.K_PERIOD and event.mod & pygame.KMOD_LSHIFT:
                self.tabplus()
            elif event.key == pygame.K_COMMA and event.mod & pygame.KMOD_LSHIFT:
                self.tabminus()
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            tab = self.tabs.checkCollide(event.pos)
            if tab >= 0:
                self.tab = tab
                self.tabchange()
        super().call(event)

        # if self.mode == 'loading':
        #     return
        # elif self.mode == 'confirmcreating':
        #     if event.type == 'KEYDOWN':
        #         if event.key == 'ESCAPE':
        #             self.mode = 'default'
        #             self.redraw()
        #         elif event.key == 'LEFT' or event.key == 'RIGHT':
        #             self.confirmButton = not self.confirmButton
        #             self.redraw()
        #         elif event.key == 'ENTER':
        #             if self.confirmButton:
        #                 self.create()
        #             else:
        #                 self.mode = 'default'
        #                 self.redraw()
        #         elif event.char == 'y' or event.char == 'Y':
        #             self.create()
        #         elif event.char == 'n' or event.char == 'N':
        #             self.mode = 'default'
        #             self.redraw()
        # elif self.mode == 'confirmescape':
        #     if event.type == 'KEYDOWN':
        #         if event.key == 'ESCAPE':
        #             self.mode = 'default'
        #             self.redraw()
        #         elif event.key == 'LEFT' or event.key == 'RIGHT':
        #             self.confirmButton = not self.confirmButton
        #             self.redraw()
        #         elif event.key == 'ENTER':
        #             if self.confirmButton:
        #                 self.returnToWorldPreview()
        #             else:
        #                 self.mode = 'default'
        #                 self.redraw()
        #         elif event.char == 'y' or event.char == 'Y':
        #             self.returnToWorldPreview()
        #         elif event.char == 'n' or event.char == 'N':
        #             self.mode = 'default'
        #             self.redraw()
        # elif self.mode == 'default':
        #     if event.type == 'KEYDOWN':
        #         if event.key == 'ESCAPE':
        #             self.mode = 'confirmescape'
        #             self.confirmButton = False
        #             self.redraw()
        #         elif event.key == 'DOWN':
        #             self.down()
        #         elif event.key == 'UP':
        #             self.up()
        #         elif event.key == 'LEFT':
        #             self.left()
        #         elif event.key == 'RIGHT':
        #             self.right()
        #         elif event.key == 'ENTER':
        #             if self.tab == 5 and event.control:
        #                 self.confirm()
        #             else:
        #                 self.enter()
        #         elif event.key == 'TAB':
        #             if event.shift:
        #                 self.tabminus()
        #             else:
        #                 self.tabplus()
        #         elif event.key == 'CHAR':
        #             if event.shift:
        #                 if event.char == '.':
        #                     self.tabplus()
        #                 elif event.char == ',':
        #                     self.tabminus()
        #         elif event.key == 'TEXT':
        #             self.text(event)
        #         elif event.key == 'BACKSPACE':
        #             self.backspace()
