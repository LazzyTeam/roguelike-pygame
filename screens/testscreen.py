from gamescreen import GameScreen
from ship import Ship, generate as generateShip, load as loadShip

import gui
import pygame
import game
from translate import _
import tile
import random
import colors
import tilemap
import directions


class TestScreen(GameScreen):
    def __init__(self):
        super().__init__()
        self.units = []
        self.player = None
        self.tilemap = tilemap.TileMap()
        self.start = (0,0)
        self.currentAction = 'move'
        self.currentActionTarget = None
        self.autoredraw = True

        ship = loadShip()
        ship.move((self.screenWidth-self.rightPanelWidth)/2-ship.rect.width/2, self.screenHeight/2-ship.rect.height/2)
        self.units.append(ship)
        self.start = (ship.x, ship.y)

        self.player = tile.Tile((self.screenWidth-self.rightPanelWidth)/2-6, self.screenHeight/2, ('@', colors.White, colors.Black, True, True, False), zIndex=1)
        self.units.append(self.player)
        self.move = False

        buttonWidth = 120
        buttonHeight = 40
        btn = gui.Button(pygame.Rect(self.screenWidth-buttonWidth-10, self.screenHeight-buttonHeight-10, buttonWidth, buttonHeight), _('Back'),
                         lambda screen, btn: game.change_screen('MainMenu'), key=pygame.K_BACKSPACE)
        self.buttons.append(btn)

        self.starfield = pygame.Surface((self.screenWidth-self.rightPanelWidth-12, self.screenHeight-12))
        for i in range(100):
            x = random.randint(0, self.screenWidth-self.rightPanelWidth-13)
            y = random.randint(0, self.screenHeight-13)
            c = (colors.LightCoral, colors.LightCyan, colors.AliceBlue, colors.Red, colors.Yellow, colors.LightSteelBlue, colors.LightYellow, colors.LightGoldenRodYellow)
            color = random.choice(c)
            self.starfield.set_at((x, y), color)


    def reset(self):
        super().reset()

    def clear(self):
        super().clear()
        game.surface.fill(colors.Black)
        pygame.draw.rect(game.staticSurface, colors.LimeGreen, (5, 5, self.screenWidth-10-self.rightPanelWidth, self.screenHeight-10), 1)
        game.surface.blit(self.starfield, (6, 6))

    def call(self, event):
        super().call(event)

        if self.currentAction == 'move':
            if event.type == pygame.KEYDOWN:
                # for unit in self.units:
                #     if unit.__class__ == Ship:
                #         for t in unit.tiles:
                #             if t.__class__ == Door:
                #                 t.action('open')
                if event.key == pygame.K_UP or event.key == pygame.K_KP8:
                    self.move = 'north'
                elif event.key == pygame.K_DOWN or event.key == pygame.K_KP2:
                    self.move = 'south'
                elif event.key == pygame.K_LEFT or event.key == pygame.K_KP4:
                    self.move = 'west'
                elif event.key == pygame.K_RIGHT or event.key == pygame.K_KP6:
                    self.move = 'east'
                elif event.key == pygame.K_KP9:
                    self.move = 'northeast'
                elif event.key == pygame.K_KP7:
                    self.move = 'northwest'
                elif event.key == pygame.K_KP1:
                    self.move = 'southwest'
                elif event.key == pygame.K_KP3:
                    self.move = 'southeast'
                elif event.key == pygame.K_e:
                    self.currentAction = 'e'
            elif event.type == pygame.KEYUP:
                if (event.key == pygame.K_UP or event.key == pygame.K_KP8) and self.move == 'north':
                    self.move = False
                elif (event.key == pygame.K_DOWN or event.key == pygame.K_KP2) and self.move == 'south':
                    self.move = False
                elif (event.key == pygame.K_LEFT or event.key == pygame.K_KP4) and self.move == 'west':
                    self.move = False
                elif (event.key == pygame.K_RIGHT or event.key == pygame.K_KP6) and self.move == 'east':
                    self.move = False
                elif event.key == pygame.K_KP9 and self.move == 'northeast':
                    self.move = False
                elif event.key == pygame.K_KP7 and self.move == 'northwest':
                    self.move = False
                elif event.key == pygame.K_KP1 and self.move == 'southwest':
                    self.move = False
                elif event.key == pygame.K_KP3 and self.move == 'southeast':
                    self.move = False
        elif self.currentAction == 'e':
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.currentAction = 'move'
                elif event.key == pygame.K_UP or event.key == pygame.K_KP8:
                    self.currentAction = 'e_target'
                    self.currentActionTarget = 'north'
                elif event.key == pygame.K_DOWN or event.key == pygame.K_KP2:
                    self.currentAction = 'e_target'
                    self.currentActionTarget = 'south'
                elif event.key == pygame.K_LEFT or event.key == pygame.K_KP4:
                    self.currentAction = 'e_target'
                    self.currentActionTarget = 'west'
                elif event.key == pygame.K_RIGHT or event.key == pygame.K_KP6:
                    self.currentAction = 'e_target'
                    self.currentActionTarget = 'east'
                elif event.key == pygame.K_KP9:
                    self.currentAction = 'e_target'
                    self.currentActionTarget = 'northeast'
                elif event.key == pygame.K_KP7:
                    self.currentAction = 'e_target'
                    self.currentActionTarget = 'northwest'
                elif event.key == pygame.K_KP1:
                    self.currentAction = 'e_target'
                    self.currentActionTarget = 'southwest'
                elif event.key == pygame.K_KP3:
                    self.currentAction = 'e_target'
                    self.currentActionTarget = 'southeast'
        elif self.currentAction == 'e_target':
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.currentAction = 'move'


    def drawRightPanel(self):
        if self.currentAction == 'e_target':
            dx, dy = directions.TILE[self.currentActionTarget]
            cx = int((self.player.x - self.start[0] + dx) // tile.TILE_SIZE)
            cy = int((self.player.y - self.start[1] + dy) // tile.TILE_SIZE)
            t = self.tilemap.get(cx, cy)
            if t and t.interactive:
                actions = t.actionList()
                if len(actions) == 1:
                    t.action(actions[0])
                    self.currentAction = 'move'
                else:
                    print(actions)
            else:
                self.currentAction = 'move'

            # render = game.font.render('asd', False, colors.LightSteelBlue)
            # game.staticSurface.blit(render, (self.screenWidth-self.rightPanelWidth+10, 10))


    def draw(self):
        super().draw()
        self.drawRightPanel()
        # unit = pygame.transform.scale(images.SPACEMAN, (images.TILE_SIZE*4, images.TILE_SIZE*4))
        # game.surface.blit(unit, (100, 100))

    def tick(self):
        super().tick()
        self.tilemap.clear()
        for unit in self.units:
            if unit.__class__ == Ship:
                for t in unit.tiles:
                    self.tilemap.set(t.x, t.y, t)
            elif unit != self.player:
                self.tilemap.set(unit.x, unit.y, unit)
        speed = 2
        if self.move and not self.player.moving:
            self.player.moving = directions.TILE[self.move]
        if self.player.moving:
            x, y = self.player.moving
            if self.canMove(x, y):
                if x > 0:
                    dx = speed
                elif x < 0:
                    dx = -speed
                else:
                    dx = 0
                if y > 0:
                    dy = speed
                elif y < 0:
                    dy = -speed
                else:
                    dy = 0
                x -= dx
                y -= dy
                if dx or dy:
                    self.player.moving = (x, y)
                elif self.move:
                    self.player.moving = directions.TILE[self.move]
                else:
                    self.player.moving = False

                self.player.move(dx, dy)
            else:
                self.player.moving = False



    def canMove(self, dx, dy):
        cx = int((self.player.x - self.start[0] + dx) // tile.TILE_SIZE)
        cy = int((self.player.y - self.start[1] + dy) // tile.TILE_SIZE)
        t = self.tilemap.get(cx, cy)
        if not t:
            return False
        else:
            return not t.blocked